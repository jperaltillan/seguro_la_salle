import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ManagePlanService {
  private apiPostData = "https://freestyle.getsandbox.com/dummy/obtenerdatospersona ";
  
  private headers = new Headers({ 'Content-Type': 'application/json' });
  constructor( 
    private http: Http) { 

    }

    

     getInfoCliente(Request: any) {
      const json = JSON.stringify(Request);
      return  this.http.post(this.apiPostData, json, { headers: this.headers }).map(res => res.json());
    }
}
