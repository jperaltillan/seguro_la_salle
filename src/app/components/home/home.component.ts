import { Component, OnInit } from '@angular/core';
import {User} from '../../Models/User'
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public _user:User  = new User();

  constructor( private route:Router) { }

  ngOnInit(): void {
  }

  InitialProcess(forma: NgForm){
    console.log();
    if(!forma.invalid){
          if(this._user.Acept1 && this._user.ACept2){
            this.route.navigate(['/Register'], 
            { queryParams: { profile: JSON.stringify(this._user) }});
          }else{
             alert("Debe aceptar los terminos");
          }

    }else{
      Object.values( forma.controls ).forEach( control => {
        control.markAsTouched();
         });
     

    }


  }


}
