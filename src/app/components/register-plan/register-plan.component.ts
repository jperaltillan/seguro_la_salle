import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {User} from '../../Models/User';
import { NgForm } from '@angular/forms';
import { General} from '../../utils/General';
import { ManagePlanService} from '../../Services/manage-plan.service'
@Component({
  selector: 'app-register-plan',
  templateUrl: './register-plan.component.html',
  styleUrls: ['./register-plan.component.scss']
})
export class RegisterPlanComponent implements OnInit {

  flgPlanes:number = 0;
  PasoNumero:number = 1;
  flgPlanSeleccionado:number =0;
  NombrePlan:string = "Mensual";
  Millones:number = 0;
  ListPlan: any = [
      { idPlan : 1, desPlan: "Basico" , desM : "2"},
      { idPlan : 2, desPlan: "Avanzado" , desM : "3"},
      { idPlan : 3, desPlan: "Premium" , desM : "4"},
      { idPlan : 4, desPlan: "Full" , desM : "5"},
  ]

  constructor(private route:Router,private activatedRoute:ActivatedRoute,private _general:General,public _managePlanService:ManagePlanService) { 
    this.activatedRoute.queryParams.subscribe(
      params => {
        this._user = <User>JSON.parse(params['profile']);
         console.log(this._user);
         this._user.Genero = "1";
         this._user.FlgAsegurar = "1";
       
      }
    )

  }
  public _user:User = new User();


  ngOnInit(): void {
    
    this.GetInfo();
  }


  finalProcess(){
    console.log(this.flgPlanSeleccionado);
    if(this.flgPlanSeleccionado == 0){
        alert("Debe seleccionar un plan !")  
    }else{
      this.flgPlanes = 2; this.PasoNumero=3;
    }
  }

  finalPlan(){
    this.flgPlanes = 2;
  }
  changePlan(){
    let item = this.ListPlan.find(x=> x.idPlan == this.flgPlanSeleccionado);
    this.NombrePlan = item.desPlan;
    this.Millones = item.desM;
    console.log(this.flgPlanSeleccionado);
  }
  GetInfo(){
    this._managePlanService.getInfoCliente(this._user).subscribe(
      (e) =>{                
          console.log(e);
      
      },
      (r) =>{ 
      console.log(r);
      }
    );
  }

  returnHome(){
    this._general.RetornarHome();
  }
  ContinueProcess(form: NgForm){
      console.log(form);
    if(!form.invalid){
      this.flgPlanes = 1;
      this.PasoNumero=2
         

    }else{
      Object.values( form.controls ).forEach( control => {
        control.markAsTouched();
         });
    }
  }

}
