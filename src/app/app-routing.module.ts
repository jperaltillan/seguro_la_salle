import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from '../app/components/home/home.component'
import {RegisterPlanComponent} from '../app/components/register-plan/register-plan.component'

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home',             component: HomeComponent },
  { path: 'Register',             component: RegisterPlanComponent },
];

@NgModule({
  imports: [
    CommonModule ,
    BrowserModule, 
    RouterModule.forRoot(routes,{
      useHash: false
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
